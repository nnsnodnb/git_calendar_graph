from bottle import default_app, route, run, static_file, template, HTTPResponse
from models import Calendar
import constants
import os
import os.path


@route('/')
def repository():
    files = os.listdir(constants.REPOSITORY_DIR)
    data = list(
        filter(
            lambda item: os.path.isdir(os.path.join(constants.REPOSITORY_DIR, item)), files
        )
    )

    return template('repository', data=data)


@route('/<repository_name>')
def contribute(repository_name):
    if not os.path.exists(f'submodules/{repository_name}') or not os.path.exists(f'submodules/{repository_name}/.git'):
        body = '<html><body>Git repository not found.</body></html>'
        r = HTTPResponse(body=body, status=404)
        r.set_header('Content-Type', 'text/html')
        return r

    calendar = Calendar(repository=repository_name)
    data = calendar.create_days_list()
    commits = calendar.create_calendar_data(data)

    return template('contributes', repository_name=repository_name, commits=commits)


@route('/static/<file_path>')
def static(file_path):
    return static_file(file_path, root='./static')


app = default_app

if __name__ == '__main__':
    run(host='0.0.0.0', reloader=True)
