import os.path


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
REPOSITORY_DIR = os.path.join(BASE_DIR, 'submodules')
