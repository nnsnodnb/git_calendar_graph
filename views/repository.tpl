<!DOCTYPE html>
<html>
    <head>
        <title>選択可能リポジトリ一覧</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        % for item in data:
            <a href="/{{ item }}">{{ item }}</a>
        % end
    </body>
</html>
