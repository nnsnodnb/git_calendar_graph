<!DOCTYPE html>
<html>
    <head>
        <title>{{ repository_name }}の年間Gitカレンダー</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="static/calendar-heatmap.css">
        <script src="static/moment.min.js"></script>
        <script src="static/d3.v3.min.js"></script>
        <script src="static/calendar-heatmap.js"></script>
    </head>
    <body>
        <h1>{{ repository_name }}の年間Gitカレンダー</h1>
        <div id="calendar" class="container"></div>
        <script type="text/javascript">
            var chartData = [];
            % for _commit in commits:
                chartData.push({
                    date: moment("{{ _commit['date'] }}").toDate(),
                    count: {{ _commit['count'] }}
                });
            % end

            var heatmap = calendarHeatmap()
                .data(chartData)
                .selector('#calendar')
                .tooltipEnabled(true)
                .colorRange(['#f4f7f7', '#af5916'])
                .onClick(function (data) {
                    console.log('data', data);
                });
            heatmap();
        </script>
    </body>
</html>
