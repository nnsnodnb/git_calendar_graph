from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from git.repo import Repo
import os.path
import constants
import multiprocessing


class Calendar(object):

    def __init__(self, repository: str, now: datetime=datetime.now()):
        self.repository = repository
        self.now = self._convert_day_first(now)
        self.year_ago = self.now - relativedelta(years=1)

    # その日の 00:00:00 を取得
    @staticmethod
    def _convert_day_first(now) -> datetime:
        return now - timedelta(hours=now.hour, minutes=now.minute, seconds=now.second)

    # 1年前から今日までの日付をstr型でlistで作成
    def create_days_list(self) -> list:
        data = []
        for day in range((self.now - self.year_ago).days + 1):
            data.append((self.year_ago + timedelta(day)).strftime('%Y-%m-%d'))

        return data

    # 指定日の総Contribute数を取得
    def _get_data_dictionary(self, date: str) -> dict:
        repo = Repo(os.path.join(constants.REPOSITORY_DIR, self.repository))
        commits = repo.iter_commits(rev='master', since=f'{date} 00:00+09:00', until=f'{date} 23:59:59+09:00')
        return {
            'date': date,
            'count': len(list(commits))
        }

    # 1年間のデータ配列を作成
    def create_calendar_data(self, data: list) -> list:
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        commits = pool.map(self._get_data_dictionary, data)
        pool.close()
        return commits
