bottle==0.12.13
gitdb2==2.0.3
GitPython==2.1.8
python-dateutil==2.6.1
six==1.11.0
smmap2==2.0.3
