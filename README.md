# git_calendar_graph

## Installation

```bash
$ git clone https://bitbucket.org/nnsnodnb/git_calendar_graph.git && cd git_calendar_graph
$ pip install -r requirements.txt
$ git submodule update
$ git submodule add https://bitbucket.org/hoge/repository.git submodules/repository
$ python app.py
```

## Author

nnsnodnb

## License

Copyright (c) 2018 Yuya Oka Released under the MIT License (see LICENSE)
